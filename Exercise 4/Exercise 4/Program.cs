﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_4
{
    class Program
    {
        static void Main(string[] args)
        {
            var num1 = 9;
            var num2 = 3;
            var num3 = num1 + num2;

            Console.WriteLine($"9+3 = {num3}" );

        }
    }
}

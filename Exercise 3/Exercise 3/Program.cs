﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_3
{
    class Program
    {
        static void Main(string[] args)
        {
            var username = " ";

            Console.WriteLine(" Hi, What is your name?");
            username = Console.ReadLine();

            Console.WriteLine($"Thank you: {username}");
        }
    }
}

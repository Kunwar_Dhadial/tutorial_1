﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Exercise_6
{
    class Program
    {
        static void Main(string[] args)
        {
            var kms = 0;
            const double miles = 0.621371;

            Console.WriteLine("Please enter the kms?");
            kms = int.Parse(Console.ReadLine());

            Console.WriteLine($"Your have entered { kms * miles} miles.");
        }
    }
}
